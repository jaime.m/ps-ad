const fs =require('fs');
const readFileAsArray = (file,cb=()=>{})=>{
    return new Promise((resolve,reject)=>{
        fs.readFile(file,(err,data)=>{
            if(err){
                reject(err)
                return cb(err);
            }
            const lines = data.toString().trim().split('\n');
            resolve(lines);
            cb(null,lines);
        });

    })
};

//example call

readFileAsArray('./numbers').then(lines=>{
    const numbers=lines.map(Number);
    const oddNumbers = numbers.filter(x=>x%2===1);
    console.log(`Odd numbers count ${oddNumbers.length}`);
}).catch(x =>{
    throw err;
})

//Work with callbacks and promises.

const countOdd=async  ()=>{
    try{
        const lines = await readFileAsArray('./numbers');
        const numbers = lines.map(Number);
        const oddCount = numbers.filter(x=>x%2===1).length;
        console.log(`Odd numbers count ${oddCount}`);
    }catch(err){
        console.error(err);
    }
}

countOdd();