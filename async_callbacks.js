const fs =require('fs');
const readFileAsArray = (file,cb)=>{
    fs.readFile(file,(err,data)=>{
        if(err){
            return cb(err)
        }
        const lines = data.toString().trim().split('\n');
        cb(null,lines);
    });
};

//example call

readFileAsArray('./numbers',(err,lines)=>{
    if(err) throw err;
    const numbers=lines.map(String);
    console.log(numbers);
    const oddNumbers = numbers.filter(x=>x%2===1);
    console.log(`Odd numbers count ${oddNumbers.length}`);
})