const {spawn} =require('child_process');

//Child implements event emitter 
const child = spawn('pwd');

child.stdout.on('data',(data)=>{
    console.log(`Child stdout:
    ${data}`);
})
child.stderr.on('data',(data)=>{
    console.log(`Child stderr:
    ${data}`);
})

child.on('exit',(code,signal)=>{
    console.log(`Child process exit with the code of ${code} and signal ${signal}`);
});



