const neg=(...args)=>{
    return args.reduce((acum,curr)=>{
        return acum-curr;
    },0)
}

console.log(neg(1,5,3));